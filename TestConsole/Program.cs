﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL;
using DAL.Models;
using DAL.Repositories;

namespace TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            //GetRaw();

            var repository = new MessageRepository();
            var result = repository.GetPagedList(0, 10).ObjectCollection;

            PrintResult(result);

            Console.ReadKey();
        }

        static void PrintResult(IEnumerable<LogMessageDto> messages)
        {
            messages.ToList().ForEach(x => Console.WriteLine(
                    "{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}",
                    x.Id,
                    x.Type,
                    x.StationName,
                    x.CommunicatorName,
                    x.RelayObject,
                    x.RelaySubObject,
                    x.RelayDevice,
                    x.ProtectedDeviceName,
                    x.TimeStamp,
                    x.Message
                    ));
        }

        static void GetRaw()
        {
            using (var db = new EtrelContext())
            {
                //rabim: LogMessage.Type
                // LogMessage.Station => Station.Name
                // LogMessage.Server => Communicator.Name
                // LogMessage.Relay => Relay.Object Relay.Subobject Relay.Device
                // LogMessage.Timestamp
                // LogMessage.Type != null => LogMessageType.Description LogMessage.Type == null => LogMessage.Message

                var result = from message in db.LogMessage
                             where message.Id == 1704835 || message.Id == 1704837
                             select new
                             {
                                 message.Id,
                                 message.Type,
                                 StationName = message.Station1.Name,
                                 CommunicatorName = message.Communicator.Name,
                                 ProtectedDeviceName = message.Relay1.Object + " / " + message.Relay1.Subobject + " / " + message.Relay1.Device,
                                 message.Timestamp,
                                 Message = message.Type != null ? message.LogMessageType.Description : message.Message
                             };

                result.ToList().ForEach(x => Console.WriteLine(
                    "{0}, {1}, {2}, {3}, {4}, {5}" +
                    "",
                    x.Type,
                    x.StationName,
                    x.CommunicatorName,
                    x.ProtectedDeviceName,
                    x.Timestamp,
                    x.Message
                    ));

                var nullIds = result.Where(x => x.Type == null);
                nullIds.ToList().ForEach(x => Console.WriteLine(x.Id));

            }
        }
    }
}
