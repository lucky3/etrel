﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PresentationWithApi.Models
{
    public class GetMessagesSearchDto
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string SearchPhrase { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
    }
}