//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class CommunicatorType
    {
        public CommunicatorType()
        {
            this.Communicator = new HashSet<Communicator>();
        }
    
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<int> TcpPort { get; set; }
        public Nullable<bool> OneToMany { get; set; }
        public bool Deleted { get; set; }
    
        public virtual ICollection<Communicator> Communicator { get; set; }
    }
}
