﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL.Models;
using PresentationWithApi.Models;

namespace DAL.Repositories
{
    public class MessageRepository
    {
        private static IQueryable<LogMessageDto> GetAll(EtrelContext db)
        {
            // Absu34jr8aa@
            //rabim: LogMessage.Type
            // LogMessage.Station => Station.Name
            // LogMessage.Server => Communicator.Name
            // LogMessage.Relay => Relay.Object Relay.Subobject Relay.Device
            // LogMessage.Timestamp
            // LogMessage.Type != null => LogMessageType.Description LogMessage.Type == null => LogMessage.Message
            var okTypes = new List<int>();
            okTypes.AddRange(new[] { 1, 4, 5, 7, 9, 11 });

            IQueryable<LogMessageDto> result = from message in db.LogMessage
                                               select new LogMessageDto
                                               {
                                                   Id = message.Id,
                                                   Type = message.Type,
                                                   StationName = message.Station1.Name,
                                                   CommunicatorName = message.Communicator.Name,
                                                   RelayObject = message.Relay1.Object,
                                                   RelaySubObject = message.Relay1.Subobject,
                                                   RelayDevice = message.Relay1.Device,
                                                   TimeStamp = message.Timestamp,
                                                   Message = message.Type != null ? message.LogMessageType.Description : message.Message,
                                                   Class = okTypes.Any(x => x == message.Type) ? true : (message.Type == null ? (bool?)null : false)
                                               };

            return result;

        }

        private IQueryable<LogMessageDto> ApplyWhereToQuery(IQueryable<LogMessageDto> query, string searchPhrase = "",
            DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            if (!string.IsNullOrEmpty(searchPhrase))
            {
                query = query.Where(x => x.Message.Contains(searchPhrase));
            }

            if (dateFrom != null)
            {
                query = query.Where(x => x.TimeStamp >= dateFrom);
            }

            if (dateTo != null)
            {
                query = query.Where(x => x.TimeStamp <= dateTo);
            }

            return query;
        }

        private IQueryable<LogMessageDto> ApplyOrderByToQuery(IQueryable<LogMessageDto> query, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            if (dateFrom != null || dateTo != null)
            {
                query = query.OrderBy(x => x.TimeStamp);
            }
            else
            {
                query = query.OrderBy(x => x.Id);
            }

            return query;
        }

        public PagedList<LogMessageDto> GetPagedList(GetMessagesSearchDto searchDto)
        {
            using (var db = new EtrelContext())
            {
                int toSkip = (searchDto.PageIndex > 0 ? searchDto.PageIndex - 1 : 0) * searchDto.PageSize;

                IQueryable<LogMessageDto> result = from message in GetAll(db) 
                                                   select message;


                result = ApplyWhereToQuery(result, searchDto.SearchPhrase, searchDto.DateFrom, searchDto.DateTo);

                int count = result.Count();



                result = ApplyOrderByToQuery(result)
                              .Skip(toSkip).Take(searchDto.PageSize);

                var pagedList = new PagedList<LogMessageDto>
                {
                    ObjectCollection = result.ToList(),
                    PageIndex = searchDto.PageIndex,
                    PageSize = searchDto.PageSize > count ? count : searchDto.PageSize,
                    TotalItems = count
                };

                return pagedList;
            }
        }
    }
}
