//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class RelayEvent
    {
        public RelayEvent()
        {
            this.EventMigration = new HashSet<EventMigration>();
        }
    
        public long Id { get; set; }
        public System.DateTime Timestamp { get; set; }
        public System.DateTime Fetched { get; set; }
        public Nullable<double> EventLocation { get; set; }
        public string ComtradeFileName { get; set; }
        public long Relay { get; set; }
        public short TransferCompleted { get; set; }
        public Nullable<System.DateTime> Timestamp_start { get; set; }
    
        public virtual ICollection<EventMigration> EventMigration { get; set; }
        public virtual Relay Relay1 { get; set; }
    }
}
