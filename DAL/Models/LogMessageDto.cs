﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class LogMessageDto
    {
        public long Id { get; set; }
        public long? Type { get; set; }
        public string StationName { get; set; }
        public string CommunicatorName { get; set; }
        public string ProtectedDeviceName { get; set; }
        public DateTime? TimeStamp { get; set; }
        public string Message { get; set; }
        public string RelayObject { get; set; }
        public string RelaySubObject { get; set; }
        public string RelayDevice { get; set; }
        public bool? Class { get; set; }
    }
}
