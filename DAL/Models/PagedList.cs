﻿using System.Collections.Generic;

namespace DAL.Models
{
    public class PagedList<T>
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int TotalItems { get; set; }
        public IEnumerable<T> ObjectCollection { get; set; }
    }
}
