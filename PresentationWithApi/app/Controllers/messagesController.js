﻿(function () {
    'use strict';

    var controllerId = 'messagesController';

    angular.module('app').controller(controllerId,
        ['$scope', '$timeout', 'messagesPagedService', messagesController]);

    function messagesController($scope, $timeout, messagesPagedService) {
        var vm = this;

        vm.activate = activate;
        vm.title = 'messagesController';
        vm.pageMessages = [];
        vm.pageIndex = 0;
        vm.pageSize = 10;
        vm.pageChanged = pageChanged;
        vm.messagesCount = null;
        vm.numPages = numPages;
        vm.setPageSize = setPageSize;
        vm.searchPhrase = '';
        vm.search = search;
        vm.openFrom = false;
        vm.openTo = false;
        vm.dateChanged = dateChanged;
        vm.timeFromPicker = new Date().setMilliseconds(0);
        vm.timeFromChanged = timeFromChanged;
        vm.timeToPicker = new Date().setMilliseconds(0);
        vm.timeToChanged = timeToChanged;
        
        var firstTimeDateQuery = true;

        function getPageMessages() {
            
            var useDateFrom = moment(vm.datePickerFromOptions.dtFrom, 'ddd MMM D YYYY HH:mm:ss ZZ').format('YYYY-MM-DD');
            var useTimeFrom = moment(vm.timeFromPicker, ['SSS', 'ddd MMM D YYYY HH:mm:ss ZZ']).format('HH:mm');

            var useDateTo = moment(vm.datePickerToOptions.dtTo, 'ddd MMM D YYYY HH:mm:ss ZZ').format('YYYY-MM-DD');
            var useTimeTo = moment(vm.timeToPicker, ['SSS', 'ddd MMM D YYYY HH:mm:ss ZZ']).format('HH:mm');

            if (firstTimeDateQuery) {
                useDateFrom = null;
                vm.datePickerFromOptions.dtFrom = null;
                vm.timeFromPicker = null;
                useDateTo = null;
                vm.datePickerToOptions.dtTo = null;
                vm.timeToPicker = null;
                firstTimeDateQuery = false;
            }

            if (vm.datePickerFromOptions.dtFrom != null && useTimeFrom === 'Invalid date') {
                useTimeFrom = moment(new Date(), ['SSS', 'ddd MMM D YYYY HH:mm:ss ZZ']).format('HH:mm');
                vm.timeFromPicker = new Date();
                
            }

            if (vm.datePickerToOptions.dtTo != null && useTimeTo === 'Invalid date') {
                useTimeTo = moment(new Date(), ['SSS', 'ddd MMM D YYYY HH:mm:ss ZZ']).format('HH:mm');
                vm.timeToPicker = new Date();
                
            }

            useDateFrom = useDateFrom + ' ' + useTimeFrom;
            useDateTo = useDateTo + ' ' + useTimeTo;


            if (useDateTo.indexOf("Invalid") != -1 || useDateTo.indexOf("null") != -1) {
                useDateTo = null;
            }

            if (useDateFrom.indexOf("Invalid") != -1 || useDateFrom.indexOf("null") != -1) {
                useDateFrom = null;
            }



            messagesPagedService.query({ pageIndex: vm.pageIndex, pageSize: vm.pageSize, searchPhrase: vm.searchPhrase, dateFrom: useDateFrom, dateTo: useDateTo }).$promise.then(function (data) {
                vm.messagesCount = data.totalItems;
                return vm.pageMessages = data.objectCollection;
            });
        }

        var keyCodes = {
            backspace: 8,
            tab: 9,
            enter: 13,
            esc: 27,
            space: 32,
            pageup: 33,
            pagedown: 34,
            end: 35,
            home: 36,
            left: 37,
            up: 38,
            right: 39,
            down: 40,
            insert: 45,
            del: 46
        };

        function setPageSize(size) {
            var middleMessageOnPage = vm.pageIndex * vm.pageSize - Math.ceil(vm.pageSize / 2);
            vm.pageSize = size;
            getNewPageIndex(middleMessageOnPage);
            getPageMessages();
        }

        var filterTextTimeout;

        function search($event) {
            if ($event.keyCode === keyCodes.esc) { vm.searchPhrase = ''; }
            if (filterTextTimeout) $timeout.cancel(filterTextTimeout);

            filterTextTimeout = $timeout(function () {
                getPageMessages();
            }, 500); // delay 500 ms
        }

        function getNewPageIndex(middleMessageOnPage) {
            numPages();
            vm.pageIndex = Math.ceil(middleMessageOnPage / vm.pageSize);
        }

        function numPages() {
            return vm.numPages = Math.ceil((vm.messagesCount / vm.pageSize) - 1);
        };

        function pageChanged(page) {
            if (!page) { return; }
            vm.pageIndex = page;
            getPageMessages();
        }

        $scope.$watch("vm.searchPhrase", function (newValue, oldValue) {
            vm.searchPhrase = newValue;
        });

        $scope.$watch("vm.timeFromPicker", function (newValue, oldValue) {
            vm.timeFromPicker = newValue;
        });

        $scope.$watch("vm.timeToPicker", function (newValue, oldValue) {
            vm.timeToPicker = newValue;
        });

        var datePickerTimeout;
        function dateChanged() {
            if (datePickerTimeout) $timeout.cancel(datePickerTimeout);

            datePickerTimeout = $timeout(function () {
                getPageMessages();
            }, 500); // delay 500 ms
        }

        var timeFromPickerTimeout;
        function timeFromChanged() {
            if (timeFromPickerTimeout) $timeout.cancel(timeFromPickerTimeout);

            timeFromPickerTimeout = $timeout(function () {
                getPageMessages();
            }, 500); // delay 500 ms
        }

        var timeToPickerTimeout;
        function timeToChanged() {
            if (timeToPickerTimeout) $timeout.cancel(timeToPickerTimeout);

            timeToPickerTimeout = $timeout(function () {
                getPageMessages();
            }, 500); // delay 500 ms
        }

        vm.datePickerFromOptions = {
            dtFrom: null,
            today: function() {
                vm.datePickerFromOptions.dtFrom = new Date();
            },
            showWeeks: true,
            toggleWeeks: function() {
                vm.datePickerFromOptions.showWeeks = vm.datePickerFromOptions.showWeeks;
            },
            clear: function() {
                vm.datePickerFromOptions.dtFrom = null;
            },
            disabled: function(date, mode) {
                return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
            },
            minDate: new Date(2010, 1, 1),
            toggleMin: function() {
                vm.datePickerFromOptions.minDate = vm.datePickerFromOptions.minDate ? null : new Date();
            },
            openFrom: function ($event) {
                $event.preventDefault();
                $event.stopPropagation();

                vm.datePickerFromOptions.openedFrom = true;
            },
            dateOptions: {
                'year-format': "'yyyy'",
                'starting-day': 1
            },
            format: 'dd.MM.yyyy'
        };

        vm.datePickerToOptions = {
            dtTo: null,
            today: function () {
                vm.datePickerToOptions.dtTo = new Date();
            },
            showWeeks: true,
            toggleWeeks: function () {
                vm.datePickerToOptions.showWeeks = vm.datePickerToOptions.showWeeks;
            },
            clear: function () {
                vm.datePickerToOptions.dtTo = null;
            },
            disabled: function (date, mode) {
                return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
            },
            minDate: new Date(2010, 1, 1),
            toggleMin: function () {
                vm.datePickerToOptions.minDate = vm.datePickerToOptions.minDate ? null : new Date();
            },
            openTo: function ($event) {
                $event.preventDefault();
                $event.stopPropagation();

                vm.datePickerToOptions.openedTo = true;
            },
            dateOptions: {
                'year-format': "'yyyy'",
                'starting-day': 1
            },
            format: 'dd.MM.yyyy'
        };

        activate();

        function activate() {
            vm.datePickerFromOptions.today();
            vm.datePickerFromOptions.toggleMin();
            vm.datePickerToOptions.today();
            vm.datePickerToOptions.toggleMin();
            getPageMessages();
            numPages();

            console.log('%c Wellcome to the Etrel test example!', 'color: springgreen; font-size: 1.5em; text-shadow: 0 1px 0 #222');
            console.log('%c Created by Janez Lukan in April 2014 ;)', 'color: red; font-size: 1.5em; text-shadow: 0 1px 0 #222');
        }
    }
})();
