﻿(function () {
    'use strict';

    var serviceId = 'messagesPagedService';

    angular.module('app').factory(serviceId, ['$http', '$resource', messagesPagedService]);

    function messagesPagedService($http, $resource) {
        return $resource('api/messages?pageIndex=:pageIndex&pageSize=:pageSize&searchPhrase=:searchPhrase&dateFrom=:dateFrom&dateTo=:dateTo', { pageIndex: '@pageIndex', pageSize: '@pageSize', searchPhrase: '@searchPhrase', dateFrom: '@dateFrom', dateTo: '@dateTo' }, {
            query: { method: 'GET', isArray: false }
        });
    }
})();