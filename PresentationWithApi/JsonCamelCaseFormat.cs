﻿using System;
using System.Web.Http.Controllers;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace PresentationWithApi
{
    public class JsonCamelCaseFormatAttribute : Attribute, IControllerConfiguration
    {
        public void Initialize(HttpControllerSettings controllerSettings, HttpControllerDescriptor controllerDescriptor)
        {
            var formatters = controllerSettings.Formatters;
            var jsonFormatter = formatters.JsonFormatter;
            var settings = jsonFormatter.SerializerSettings;
            settings.Formatting = Formatting.Indented;
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            // Ensure that we NEVER return XML.
            if (controllerSettings.Formatters.XmlFormatter != null)
            {
                controllerSettings.Formatters.Remove(controllerSettings.Formatters.XmlFormatter);
            }
        }
    }
}