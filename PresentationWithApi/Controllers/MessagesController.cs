﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using DAL.Models;
using DAL.Repositories;
using PresentationWithApi.Models;

namespace PresentationWithApi.Controllers
{
    [JsonCamelCaseFormat] 
    public class MessagesController : ApiController
    {
        readonly MessageRepository _repository = new MessageRepository();

        private static readonly GetMessagesSearchDto DefaultSearch = new GetMessagesSearchDto
        {
            PageIndex = 0,
            PageSize = 10
        };

        // GET api/messages?pageIndex=1&pageSize=10
        public PagedList<LogMessageDto> Get([FromUri] GetMessagesSearchDto searchDto)
        {
            searchDto = searchDto ?? DefaultSearch;

            if (searchDto.PageSize <= 0)
            {
                searchDto.PageSize = 10;
            }

            PagedList<LogMessageDto> result = _repository.GetPagedList(searchDto);
            return result;
        }
    }
}
