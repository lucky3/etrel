﻿(function () {
    'use strict';

    var id = 'app';

    var app = angular.module(id, [
        // Angular modules 
        'ngAnimate',        // animations
        'ngRoute',           // routing
        'ngResource',
        'ngSanitize',

        // Custom modules 
        
        // 3rd Party Modules
        'ui.bootstrap',
        'chieffancypants.loadingBar'
    ]);
})();